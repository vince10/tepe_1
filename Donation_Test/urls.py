from django.urls import path
from .views import index,display_donators,make_donations,tembak_json

urlpatterns = [
   path('donate/',index, name='donate'),
   path('display_donators/',display_donators,name='display_donators'),
   path('makedonation/',make_donations,name='make_donations'),
   path('tembak_json/',tembak_json,name='tembak_json')
]