from django.db import models
# Create your models here.

class InformasiPalu (models.Model):
    title = models.CharField(max_length = 200, default = "title")
    donation_target = models.IntegerField(default=0)
    content = models.CharField(max_length=2000, default = "content")


class InformasiLombok (models.Model):
    title = models.CharField(max_length = 200, default = "title")
    donation_target = models.IntegerField(default=0)
    content = models.CharField(max_length=2000, default = "content")

