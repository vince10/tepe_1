from django.apps import AppConfig


class NewsroomConfig(AppConfig):
    name = 'Newsroom'
