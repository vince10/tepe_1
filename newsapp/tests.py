from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import newsapp, newsapp2
from .models import InformasiLombok, InformasiPalu

# Create your tests here.
class tpSatuUnitTest(TestCase):
    def test_url_palu_is_exist(self):
        response = Client().get('/news/palu/')
        self.assertEqual(response.status_code, 200)

    def test_using_newsapp_palu_func(self):
        found = resolve('/news/palu/')
        self.assertEqual(found.func, newsapp)

    def test_using_landing_page_palu_template(self):
        response = Client().get('/news/palu/')
        self.assertTemplateUsed(response, 'palu.html')

    def test_url_lombok_is_exist(self):
        response = Client().get('/news/lombok/')
        self.assertEqual(response.status_code, 200)

    def test_using_newsapp_lombok_func(self):
        found = resolve('/news/lombok/')
        self.assertEqual(found.func, newsapp2)

    def test_using_landing_page_lombok_template(self):
        response = Client().get('/news/lombok/')
        self.assertTemplateUsed(response, 'lombok.html')
