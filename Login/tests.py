from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Donatur

# Create your tests here.

class Login_Test(TestCase):
    

    def test_if_url_and_template_is_exists(self):
        response = Client().get('/user_login/login/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'login.html')

        #test buat logoutnya juga
        response = Client().get('/user_login/logout/')
        self.assertEqual(response.status_code,302)
    
    def can_create_object(self):
        donatur = Donatur.objects.create(
            name="Igor",
            email="adi.vanbasten99@gmail.com"
        )
        self.assertEqual(Donatur.objects.all().count(),1)
    
    

    
    
 




    