from django.shortcuts import render
from .models import News

# Create your views here.
response = {}

def Newsroom (request):
    response['newsroom']= News.objects.all()
    return render(request,'newsroom.html', response)