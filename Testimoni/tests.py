from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps

from .apps import TestimoniConfig
from .views import indexs
from .models import TestimoniModel

class TestiAppTest(TestCase):
    def test_apps_config(self):
        self.assertEqual(TestimoniConfig.name, 'Testimoni')
        self.assertEqual(apps.get_app_config('Testimoni').name, 'Testimoni')
    
class Testimoni_Test(TestCase):
    def test_if_url_and_template_are_exist(self):
        response = Client().get('/testimoni/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'testimoni.html')

    def test_if_function_for_html_rendering_exists(self):
        found = resolve('/testimoni/')
        self.assertEqual(found.func,indexs)

class TestiModelTest(TestCase):
    def test_default_attr(self):
        testi = TestimoniModel()
        self.assertEqual(testi.namaTestimoni, '')
        self.assertEqual(testi.testimoni, '')

    def test_testi_verbose_name_plural(self):
        self.assertEqual(str(TestimoniModel._meta.verbose_name_plural), "testimoni models")

    def test_model_can_create_new_testi(self):
        new_testi = TestimoniModel.objects.create(namaTestimoni="coba", testimoni=":)")
        all_available_testi = TestimoniModel.objects.all().count()
        self.assertEqual(all_available_testi, 1)
        
class FormTestimoni_Test(TestCase):
    def test_success_insert_testimoni(self):
        name = 'CobaCoba'
        testi = ':)'
        response = Client().post('/testimoni/showTestimoni/', {"namaTestimoni": "CobaCoba", "testimoni": ":)"})

        self.assertEqual(response.status_code, 200)
