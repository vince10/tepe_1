from django.db import models

# Create your models here.
class Donation(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=70, null=True, blank=True,default="")
    #password =  models.CharField(max_length=100,default="")
    amount = models.IntegerField()
    location = models.CharField(max_length=200,default="")
    program = models.CharField(max_length=200)
    anonymous =  models.CharField(max_length=10,default='YES')
   