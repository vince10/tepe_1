from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index,add_donatur
from .models import Register
from .forms import Register_Form
# Create your tests here.
class Register_Test(TestCase):
    
    def test_if_is_exists(self):
        response = Client().get('/register_test/register/')
        self.assertEqual(response.status_code,200)
    def test_using_register_index_function(self):
        found=resolve('/register_test/register/')
        self.assertEqual(found.func,index)
    def test_using_register_page_template(self):
        response = Client().get('/register_test/register/')
        self.assertTemplateUsed(response,'form_test.html')
    def test_model_can_create_Register(self):
        register = Register.objects.create(
            name="Igor",
            dob="1999-01-01",
            email="ariqharyo29@gmail.com",
            password="password",
            password_verif="password"
        )
        counting_all_register = Register.objects.all().count()
        self.assertEqual(counting_all_register,1)
    
    def test_register_form__validation_for_blank_items(self):
        form = Register_Form(data={
            'name':"",
            'dob':"1999-01-01",
            'email':"ariqharyo29@gmail.com",
            'password':"password",
            'password_verif':"password"
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ['This field is required.']
        )
    
    def test_email_validation_fail(self):
        test = "Apparently"
        user_1 =  Register.objects.create(
            name="Igor",
            dob="1999-01-01",
            email="ariqharyo29@gmail.com",
            password="password",
            password_verif="password"
         )
        counting_all_user = Register.objects.all().count()
        #email = "ariqharyo29@gmail.com"
        response_post = Client().post('/register_test/add_donatur/',
                        {'name':"Igorrr",
             'dob':"1999-01-01",
             'email':  "ariqharyo29@gmail.com",
             'password':"password",
             'password_verif':"password"})
        self.assertEqual(response_post.status_code,
        302)
        self.assertEqual(counting_all_user,1)

        response = Client().get('/register_test/register/')
        html_response = response.content.decode('utf8')
        self.assertIn(test,html_response)



        # user_1 =  Register.objects.create(
        #     name="Igor",
        #     dob="1999-01-01",
        #     email="ariqharyo29@gmail.com",
        #     password="password",
        #     password_verif="password"
        # )
        # email = "ariqharyo29@gmail.com"
        # form = Register_Form(data={
        #     'name':"Igorrr",
        #     'dob':"1999-01-01",
        #     'email': email,
        #     'password':"password",
        #     'password_verif':"password"
        # })
        # counting_all_user = Register.objects.all().count()
        # self.assertEqual(email,user_1.email)
        # self.assertEqual(counting_all_user,1)

        # if (form.email != user_1.email):
        #     user_2 = Register.objects.create(
        #     name="Igorrr",
        #     dob="01/01/1999",
        #     email="ariqharyo28@gmail.com",
        #     password="password",
        #     password_verif="password"
        # )
        #     counting_all_user = Register.objects.all().count()
        #     self.assertEqual(counting_all_user,2)
        # else :
        #     counting_all_user = Register.objects.all().count()
        #     self.assertEqual(counting_all_user,1)
    def test_register_validation_success(self):
        test = "Registration Success"
        user_1 =  Register.objects.create(
            name="Igor",
            dob="1999-01-01",
            email="ariqharyo30@gmail.com",
            password="password",
            password_verif="password"
        )
        counting_all_user = Register.objects.all().count()
        response_post = Client().post('/register_test/add_donatur/',
                        {'name':"Igorrr",
            'dob':"1999-01-01",
            'email':"ariqharyo31@gmail.com",
            'password':"password",
            'password_verif':"password"})
        self.assertEqual(response_post.status_code,
        302)
        
        response = Client().get('/register_test/register/')
        html_response = response.content.decode('utf8')
        self.assertIn(test,html_response)
        
    def test_password_validation_fail(self):
        test = "Apparently"
        # password = "password"
        # password_verif = "passworddd"
        response_post = Client().post('/register_test/add_donatur/',
                        {'name':"Igorrr",
            'dob':"1999-01-01",
            'email':"ariqharyo32@gmail.com",
            'password':"password",
            'password_verif':"passwordddd"})
        self.assertEqual(response_post.status_code,
        302)
        
        response = Client().get('/register_test/register/')
        html_response = response.content.decode('utf8')
        self.assertIn(test,html_response)
    # def test_password_validation_success(self):
    #     password = "password"
    #     password_verif = "password"
    #     form = Register_Form(data={
    #         'name':"Igorrr",
    #         'dob':"1999-01-01",
    #         'email':"ariqharyo27@gmail.com",
    #         'password':password,
    #         'password_verif':password_verif
    #     })
    #     user_1 =  Register.objects.create(
    #         name="Igorrr",
    #         dob="1999-01-01",
    #         email = "ariqharyo27@gmail.com",
    #         password = password,
    #         password_verif = password_verif
    #     )


    #     counting_all_user = Register.objects.all().count()
    #     self.assertEqual(password,password_verif)
    #     self.assertEqual(counting_all_user,1)
    




        
    



