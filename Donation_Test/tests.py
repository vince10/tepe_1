from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index,display_donators,tembak_json
from .models import Donation
from .forms import Donation_Form
from Register_Test.models import Register
from django.contrib.auth.models import User



# Create your tests here.
class Donation_Test(TestCase):

    

    # def setUp(self):
    #     user = User.objects.create(
    #         username='adivanbasten',
    #         email='adi.vanbasten99@gmail.com',
    #         password='akucintappwselamanya'
    #     )
    #     self.user = user
    
    #pass

    def test_if_is_exists(self):
        response = Client().get('/donation_test/donate/')
        self.assertEqual(response.status_code,302)
    def test_tembak_json(self):
        found=resolve('/donation_test/tembak_json/')
        self.assertEqual(found.func,tembak_json)
        response = Client().get('/donation_test/tembak_json')
        test = ""
        resp = response.content.decode('utf8')
        self.assertIn(test,resp)
    def test_create_objects(self):
        obj = Donation.objects.create(
            name="joko",
            email="widodo@gmail.com",
            amount=100,
            location="PALU",
            program="PROGRAM 1",
            anonymous='YES'
        )
        count = Donation.objects.all().count()
        self.assertEqual(count,1)

  
    



    

