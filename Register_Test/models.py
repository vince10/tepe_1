from django.db import models

# Create your models here.
class Register(models.Model):
    name = models.CharField(max_length=200)
    dob = models.DateField()
    email = models.EmailField(max_length=70, null=True, blank=True)
    password =  models.CharField(max_length=100)
    password_verif = models.CharField(max_length=100)