from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect,HttpResponse

from django.http import JsonResponse
from .models import TestimoniModel
from django.core import serializers
import json
# Create your views here.

def indexs(request):
    return render(request,'testimoni.html')

# def testimoni(request):
# def testimoni (request):
#     nama = request.GET.get('namaTestimoni', None)
#     isi  = request.GET.get('testimoni', None)
#     validate_desc = not (len(desc) == 0 or desc.isspace())

#     #Jika sudah terautentikasi dan tervalidasi(sudah login), model membuat modelnya
#     if request.user.is_authenticated and validate_desc:
#         TestimoniModel.objects.create (namaTestimoni=nama, testimoni=isi)

#         buatkonten = TestimoniModel.objects.all()
#         testimoniModel.objects.create (namaTestimoni=nama, testimoni=isi)

#         buatkonten = testimoniModel.objects.all()
#         response = []
#         for i in buatkonten :
#             response.append({
#                 'namaTestimoni' : i.namaTestimoni,
#                 'testimoni'  : i.testimoni
#             }) 

#         return JsonResponse(response, safe=False)

def testimoni_handler(request):
    if (request.method=="POST"):
        testimoni = TestimoniModel.objects.create(
            namaTestimoni=request.user.first_name,
            testimoni=request.POST['testimoni']
        )
        return redirect('Testimoni:testimoni')
    return redirect('Testimoni:testimoni')

def tembak_json_testi(request):
    all_testi = TestimoniModel.objects.all()
    print(all_testi.count())
    the_json = {'all_testimoni':all_testi}
    the_json = serializers.serialize("json",all_testi)
    the_json = {'all_testimoni':the_json}
    return JsonResponse(the_json,safe=False)
    # return JsonResponse(json.loads(the_json))

    # pass


def createTestimoni(request):
    if request.method == "POST" and request.is_ajax():
        json_data = json.loads(request.body, encoding='UTF-8')
        TestimoniModel.objects.create(
            namaTestimoni = json_data['namaTestimoni'],
            testimoni = json_data['testimoni']
        )
        return HttpResponse("New testi successfully added", status=201)
    else:
        return HttpResponse("Failed", status=405)

def showTestimoni(request):
    testimoni = TestimoniModel.objects.all()
    print(testimoni)
    listOfTesti = []

    for testi in testimoni:
        data = {'namaTestimoni' : testi.namaTestimoni, 'testimoni' : testi.testimoni}
        listOfTesti.append(data)

    return JsonResponse(listOfTesti, safe=False)

# def shootJSON(request):
#     all_testimoni = testimoniModel.objects.all()
#     json_data = {'all_testimonis' : all_testimoni}
#     json_data = serializers.serialize("json", all_testimoni)
#     return JsonResponse(json_data, safe=False)

# def createTestimoni(request):
#     if (request.method=="POST" and request.user.is_authenticated):
#         testimoni = testimoniModel.objects.create(
#             namaTestimoni = request.user.username,
#             testimoni = request.POST['testimoni']
#         )

#         testimoni = serializers.serialize("json",[testimoni])
#         print(testimoni)
#         return redirect('Testimoni:showTestimoni')
#     return redirect('Testimoni:indexs')

# def showTestimoni(request):
#     response['listOfTestimoni'] = testimoniModel.objects.all()
#     return render(request, 'testimoni.html', response)
