from django import forms
from .models import Register
#from django.core.validators import email_re
from django.core.exceptions import ValidationError


class Register_Form(forms.ModelForm):
    #dob = forms.DateInput(attrs={'type': 'date'}, label="Date of Birth")
    password = forms.CharField( 
    widget=forms.PasswordInput(render_value=False), 
    label="Your Password")
    password_verif = forms.CharField( 
    widget=forms.PasswordInput(render_value=False), 
    label="Verify Your Password"
)
    class Meta:
        model = Register
        fields =[
            'name',
            'dob',
            'email',
            'password',
            'password_verif',

        ]
        widgets = {
            'dob': forms.DateInput(attrs={'type': 'date'}),
        }

    # def bersih_email(self):
    #     #   print("berhasil masuk ke clean_email")
    #       print("keeksekusi bersih_email")
    #       data = self.cleaned_data['email']
    #       if Register.objects.filter(email=data).count() > 0:
    #           raise forms.ValidationError("We have a user with this user email-id")
    #       return data
    # # def save(self, *args, **kwargs):
    # #     # ... other things not important here
    # #     self.email = self.email.lower().strip() # Hopefully reduces junk to ""
    # #     if self.email != "": # If it's not blank
    # #         if not email_re.match(self.email): # If it's not an email address
    # #             raise ValidationError(u'%s is not an email address, dummy!' % self.email)
    # #     if self.email == "":
    # #         self.email = None
    # #     super(Register, self).save(*args, **kwargs)
    

    # def bersih(self):
    #     # print("berhasil masuk ke clean")
    #     print("keeksekusi bersih")
    #     form_data = self.cleaned_data
    #     if form_data['password'] != form_data['password_verif']:
    #         self._errors["password"] = ["Password do not match"] # Will raise a error message
    #         del form_data['password']
    #     return form_data


    

