from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

class HomepageTest(TestCase):
    def test_tugas1_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_tugas1(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_tugas1_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)


# class Login_Test(TestCase):
#     def test_if_url_and_template_are_exist(self):
#         response = Client().get('/login/')
#         self.assertEqual(response.status_code,200)
#         self.assertTemplateUsed(response,'login.html')
#     def test_if_function_for_html_rendering_exists(self):
#         found = resolve('/login/')
#         self.assertEqual(found.func,login)

#     def test_success_login(self):
#         pass
    
    
