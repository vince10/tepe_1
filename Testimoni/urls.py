from django.urls import path
from django.conf.urls import url
from .views import indexs,testimoni_handler,tembak_json_testi,createTestimoni,showTestimoni
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
   path('',indexs, name='testimoni'),
#    url(r'^testimoni/', testimoni, name= 'testimonii'),
   path('write_testi/',testimoni_handler,name='write_testi'),
   path('json_testi/',tembak_json_testi,name='json_testi'),
#    path('',indexs, name='indexs'),
   # url(r'^testimoni/', testimoni, name= 'testimonii')
   url(r'^createTestimoni/', createTestimoni, name='createTestimoni'),
   url(r'^showTestimoni/', showTestimoni, name='showTestimoni'),
]

urlpatterns += staticfiles_urlpatterns()