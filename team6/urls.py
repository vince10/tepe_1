"""team6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include(('homepage.urls','homepage'),namespace='homepage')),
    path('helloworld/',include(('Hello_World.urls','Hello_World'),namespace='Hello_World')),
    path('register_test/',include(('Register_Test.urls','Register_Test'),namespace='Register_Test')),
    path('donation_test/',include(('Donation_Test.urls','Donation_Test'),namespace='Donation_Test')),
    path('news/',include(('newsapp.urls','newsapp'),namespace='newsapp')),
    path('newsroom/',include(('Newsroom.urls','Newsroom'),namespace='Newsroom')),
    path('testimoni/',include(('Testimoni.urls','Testimoni'),namespace='Testimoni')),
    path('auth/',include('social_django.urls', namespace='social')),
    path('user_login/',include(('Login.urls','Login'),namespace='Login'))
	
    #path('donation_test/',include(('Donation_Test.urls','Donation_Test'),namespace='Donation_Test'))

]
