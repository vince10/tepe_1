# Generated by Django 2.1.1 on 2018-10-17 11:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Donation_Test', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='donation',
            name='anonymous',
            field=models.CharField(default='YES', max_length=10),
        ),
    ]
