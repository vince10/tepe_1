from django import forms
from .models import Donation
from Login.models import Donatur
from Register_Test.forms import Register_Form
from Register_Test.models import Register
#from django.core.validators import email_re
from django.core.exceptions import ValidationError

class Donation_Form(forms.ModelForm):
    class Meta:
        model = Donation
        fields = [#'name',
                #'email',
                #'password',
                'amount',
                'location',
                'program',
                'anonymous'
        ]
        PROGRAM_CHOICES = (
                ('', 'Select the Program'),
                ('Program 1', 'Program 1'), #First one is the value of select option and second is the displayed value in option
                ('Program 2', 'Program 2'),
                ('Program 3', 'Program 3'),
                ('Program 4', 'Program 4'),
                )
        ANONYMOUS = (
               
                ('YES', 'YES'), #First one is the value of select option and second is the displayed value in option
                ('NO', 'NO'),
                )
        LOCATION_CHOICES = (
                ('', 'Select the Location'),
                ('Palu', 'Palu'), #First one is the value of select option and second is the displayed value in option
                ('Lombok', 'Lombok'),
                )

        widgets = {
            #'password':forms.PasswordInput(render_value=False),
            'amount':forms.NumberInput(
                attrs={'size':'15'}),
            'location':forms.Select(choices = LOCATION_CHOICES,attrs={'class':'form-control'}),
            'program':forms.Select(choices=PROGRAM_CHOICES,attrs={'class': 'form-control'}),
            'anonymous':forms.Select(choices=ANONYMOUS,attrs={'class': 'form-control'})
            
        }

    # def check_email_and_pass(self):
    #     print("keeksekusi check email pass")
    #     all_user = Register.objects.all()
    #     form_data = self.cleaned_data
    #     name = form_data['name']
    #     password = form_data['password']
    #     foundit = False
    #     for data in all_user:
    #         print(data.password)
    #         print(data.name)
    #         if (data.password==password and data.name==name):
    #             print("foundit")
    #             foundit = True
    #             break
    #     if (foundit==False):
    #         raise forms.ValidationError("Wrong name and password, Please check your input again!")
    #     return form_data
