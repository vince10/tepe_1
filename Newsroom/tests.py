from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import Newsroom
from .models import News

# Create your tests here.


class tpSatuUnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/newsroom/')
        self.assertEqual(response.status_code, 200)

    def test_using_newsapp_func(self):
        found = resolve('/newsroom/')
        self.assertEqual(found.func, Newsroom)

    def test_using_landing_page_template(self):
        response = Client().get('/newsroom/')
        self.assertTemplateUsed(response, 'newsroom.html')
