from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import helloworld

# Create your tests here.

class Tepe1_test(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/helloworld/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_helloworld_func(self):
        found = resolve('/helloworld/')
        self.assertEqual(found.func, helloworld)

    def test_using_landing_page_template(self):
        response = Client().get('/helloworld/')
        self.assertTemplateUsed(response, 'hellow_world.html')
