# Generated by Django 2.1.1 on 2018-12-10 11:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Login', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='donatur',
            name='amount',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='donatur',
            name='anonymous',
            field=models.CharField(default='YES', max_length=10),
        ),
        migrations.AddField(
            model_name='donatur',
            name='location',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='donatur',
            name='program',
            field=models.CharField(default='Palu', max_length=200),
        ),
    ]
