from django.shortcuts import render,redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib import messages
from .models import Donation
from .forms import Donation_Form
from Register_Test.models import Register
from django.http import JsonResponse
from django.core import serializers

# Create your views here.

response = {}
donations = {}
#donations['donations_made']=[]

def index(request):
    sudah_login = request.user.is_authenticated
    if (not sudah_login):
         return redirect('Login:login')
    response['donation_form'] = Donation_Form
    return render(request, 'donations.html', response)


def tembak_json(request):
    all_donation = Donation.objects.filter(email=request.user.email)
    the_json = {'all_donations':all_donation}
    the_json = serializers.serialize("json",all_donation)
    the_json = {'all_donations':the_json}
    return JsonResponse(the_json,safe=False)

def make_donations(request):
    if (request.method=="POST" and request.user.is_authenticated and  int(request.POST['amount'])>=0):
        donation = Donation.objects.create(
            name = request.user.username,
            email = request.user.email,
            amount = request.POST['amount'],
            location = request.POST['location'],
            program = request.POST['program'],
            anonymous = request.POST['anonymous']
        )
        donation = serializers.serialize("json",[donation])
        print(donation)
        return redirect('Donation_Test:display_donators')
    return redirect('Donation_Test:donate')
        #donation['d']

    
       

def display_donators(request):
    sudah_login = request.user.is_authenticated
    if (not sudah_login):
         return redirect('Login:login')
    response['all_donators'] = Donation.objects.all()
    return render(request, 'the_donators.html',response)

def count_the_money(location):
    temp = 0
    var = Donation.objects.all()
    for data in var:
        if (data.location == location):
            temp+=data.amount
    return temp

