from django.urls import path
from .views import Newsroom

urlpatterns = [
   path('',Newsroom, name='Newsroom'),
]